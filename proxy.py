import asyncio
from dataclasses import dataclass
from sys import argv
import uvloop


@dataclass
class Pixel:
    x: int
    y: int
    color: str


def encode(pixel: Pixel) -> str:
    """ encode pixel, but with shorter color code, because the
        controller display only uses 4 bits per RGB color. instead
        of the usual 8. """
    color = "".join(
        [hex(byte >> 4)[2:] for byte in bytearray.fromhex(pixel.color)]
    )
    return f"PX {str(pixel.x).rjust(3,'0')} {str(pixel.y).rjust(3,'0')} {color}\n"


def decode(raw_string: str) -> Pixel:
    # decode pixel flut message
    values = raw_string.split(" ")
    pixel = Pixel(x=int(values[1]), y=int(values[2]), color=values[3].strip())
    return pixel


async def handle_echo(reader, writer):
    addr = writer.get_extra_info("peername")
    print(f"New connection from {addr}")
    data = b""
    while True:
        c = await reader.read(1)
        if c == b"":
            print("Connection Closed")
            return
        if c == b"\n":
            message = data.decode()
            data = b""
            if "SIZE" == message:
                writer.write(b"SIZE 160 80\n")
                await writer.drain()
            elif "PX" in message:
                pixel = decode(message)
                encoded = encode(pixel)
                tty.write(encoded)
        else:
            data += c





USAGE = """
    Pixel flut proxy for microcontroller

    Usage: sudo python proxy.py <port> <tty-device>

    Example: sudo python proxy.py 8080 /dev/ttyACM0
"""

assert len(argv) == 3, USAGE
assert int(argv[1]), USAGE
assert isinstance(argv[2], str), USAGE


tty = open(argv[2], "w")

asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
loop = asyncio.get_event_loop()
coro = asyncio.start_server(handle_echo, "0.0.0.0", int(argv[1]), loop=loop)
server = loop.run_until_complete(coro)

# Serve requests until Ctrl+C is pressed
print("Serving on {}".format(server.sockets[0].getsockname()))
try:
    loop.run_forever()
except KeyboardInterrupt:
    pass

# Close the server
server.close()
loop.run_until_complete(server.wait_closed())
loop.close()
